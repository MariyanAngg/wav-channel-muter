#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef struct {
    char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
    short audio_format;
    short num_channels;
    int sample_rate;
    int byte_rate;
    short block_align;
    short bits_per_sample;
    char subchunk2_id[4];
    int subchunk2_size;
} WavHeader;

const char* WAV_FILE_PATH = "file1.wav";
const char* OUTPUT_FILE_PATH = "result.txt";

char select_chunk_size_unit(int chunk_size) {
    if (chunk_size < 1024) {
        return 'B';
    }

    if (chunk_size < 1024 * 1024) {
        return 'K';
    }

    if (chunk_size < 1024 * 1024 * 1024) {
        return 'M';
    }

    if (chunk_size > 1024 * 1024 * 1024) {
        return 'G';
    }
}

void print_chunk_size(int chunk_size, char unit) {
    if (unit == '?') {
        unit = select_chunk_size_unit(chunk_size);
    }

    switch(unit) {
        case 'B':
            printf("The sound content is %d B", chunk_size);
            break;

        case 'K':
            printf("The sound content is %d KB", chunk_size / 1024);
            break;

        case 'M':
            printf("The sound content is %d MB", chunk_size / 1024 / 1024);
            break;
    }
}

int main(int argc, char **argv) {

    // Process input

    int is_silent = 0;
    char file_path[32];
    char unit = '?';

    char opt = -1;
    while ((opt = getopt (argc, argv, "sf:u:")) != -1) {
        switch (opt) {
            case 's':
                is_silent = 1;
                continue;

            case 'f':
                strcpy(file_path, optarg);
                continue;

            case 'u':
                unit = optarg[0];
                if (unit != 'B' && unit != 'K' && unit != 'M') {
                    fprintf(stderr, "Invalid option given -u=%s", unit);
                    return 1;
                }

                continue;

            default:
                fprintf(stderr, "Unknown option given: -%s", opt);
                return 1;
        }
    }

    // Read .WAV file contents

    printf("Reading file: %s\n", WAV_FILE_PATH);
    FILE *file = fopen(WAV_FILE_PATH, "rb");
    if (!file) {
        fprintf(stderr, "Error: Could not open .WAV file '%s'\n", WAV_FILE_PATH);
        return 1;
    }

    WavHeader header;
    fread(&header, sizeof(header), 1, file);

    if (strncmp(header.chunk_id, "RIFF", 4) != 0 || strncmp(header.format, "WAVE", 4) != 0) {
        fprintf(stderr, "Error: The selected file '%s' is not a valid WAV file\n", file_path);
        return 1;
    }

    // Print file info

    printf("Number of channels: %d\n", header.num_channels);
    print_chunk_size(header.chunk_size, unit);

    // Mute channels
    if (header.num_channels == 2) {
        char* choice = NULL;
        int len = 0;
        getline(&choice, &len, stdin);

        if (strcmp(choice, "left") == 0) {
            // Create output file
            FILE *out_file = fopen(OUTPUT_FILE_PATH, "wb");
            if (!out_file) {
                fprintf(stderr, "Error: Could not open output file '%s'\n", OUTPUT_FILE_PATH);
                return 1;
            }

            // Copy header
            fwrite(&header, sizeof(header), 1, out_file);

            // Copy sound content with left channel muted
            short sample;
            for (int i = 0; i < header.subchunk2_size / header.block_align; i++) {
                fread(&sample, sizeof(short), 1, file);
                if (i % header.num_channels == 0) {
                    sample = 0;
                }
                fwrite(&sample, sizeof(short), 1, out_file);
            }

            fclose(out_file);
            printf("Done. The result file is saved at '%s'.\n", OUTPUT_FILE_PATH);
        } else if (strcmp(choice, "right") == 0) {
            // Create output file
            FILE *out_file = fopen(OUTPUT_FILE_PATH, "wb");
            if (!out_file) {
                fprintf(stderr, "Error: Could not open output file '%s'\n", OUTPUT_FILE_PATH);
                return 1;
            }

            // Copy header
            fwrite(&header, sizeof(header), 1, out_file);

            // Copy sound content with right channel muted
            short sample;
            for (int i = 0; i < header.subchunk2_size / header.block_align; i++) {
                fread(&sample, sizeof(short), 1, file);
                if (i % header.num_channels == 1) {
                    sample = 0;
                }
                fwrite(&sample, sizeof(short), 1, out_file);
            }

            fclose(out_file);
            printf("Done. The result file is saved at '%s'.\n", OUTPUT_FILE_PATH);
        } else {
            printf("Invalid choice. Please enter either 'left' or 'right'.\n");
        }
    }
}

